package com.solusys.gwt.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.github.javafaker.Faker;
import com.solusys.gwt.api.dto.ContactDTO;

@Entity
public class Contact {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	private String firstName;

	private String lastName;

	private String emailAddress;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	@Override
	public int hashCode() {
		return Objects.hash(emailAddress, firstName, id, lastName);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Contact other = (Contact) obj;
		return Objects.equals(emailAddress, other.emailAddress) && Objects.equals(firstName, other.firstName)
				&& Objects.equals(id, other.id) && Objects.equals(lastName, other.lastName);
	}

	@Override
	public String toString() {
		return "Contact [id=" + id + ", firstName=" + firstName + ", lastName=" + lastName + ", emailAddress="
				+ emailAddress + "]";
	}

	public ContactDTO toContactDTO() {
		ContactDTO dto = new ContactDTO();
		dto.setId(id.toString());
		dto.setFirstName(this.firstName);
		dto.setLastName(this.lastName);
		dto.setEmailAddress(this.emailAddress);
		return dto;
	}

	public void updateFromDTO(ContactDTO dto) {
		this.id = Long.valueOf(dto.getId());
		this.firstName = dto.getFirstName();
		this.lastName = dto.getLastName();
		this.emailAddress = dto.getEmailAddress();
	}

	public Contact(ContactDTO contactDTO) {
		this.id = Long.parseLong(contactDTO.getId());
		this.firstName = contactDTO.getFirstName();
		this.lastName = contactDTO.getLastName();
		this.emailAddress = contactDTO.getEmailAddress();
	}

	public Contact() {
		// TODO Auto-generated constructor stub
	}

	static public Contact demoContact() {
		Contact contact = new Contact();
		contact.firstName = "Bob";
		contact.lastName = "Smith";
		contact.emailAddress = "bsmith@example.com";
		return contact;
	}

	private static final Faker faker = new Faker();

	static public List<Contact> randomContacts(int count) {

		List<Contact> result = new ArrayList<>();

		for (int i = 0; i < count; i++) {
			Contact contact = new Contact();
			contact.id = (long) faker.hashCode();
			contact.firstName = faker.name().firstName();
			contact.lastName = faker.name().lastName();
			contact.emailAddress = faker.internet().safeEmailAddress();
			result.add(contact);
		}

		return result;
	}

}