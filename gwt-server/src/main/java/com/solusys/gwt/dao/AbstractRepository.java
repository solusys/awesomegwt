package com.solusys.gwt.dao;

import java.util.List;

public interface AbstractRepository<T, ID> {
	T save(T entity);

	List<T> findAll();

	T findById(ID id);

	void delete(T entity);

	void deleteById(ID id);
}
