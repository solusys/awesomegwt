package com.solusys.gwt.dao;

import java.util.List;

import com.solusys.gwt.model.Contact;

public interface ContactRepository extends AbstractRepository<Contact, Long> {
	List<Contact> findByFirstName(String firstName);

	List<Contact> findByLastName(String lastName);
}
