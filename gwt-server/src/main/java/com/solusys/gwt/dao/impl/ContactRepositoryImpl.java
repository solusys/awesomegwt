package com.solusys.gwt.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.solusys.gwt.dao.ContactRepository;
import com.solusys.gwt.model.Contact;

@Repository
@Transactional
public class ContactRepositoryImpl implements ContactRepository {

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public List<Contact> findByFirstName(String firstName) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);
		Root<Contact> root = cq.from(Contact.class);
		Predicate predicate = cb.equal(root.get("firstName"), firstName);
		cq.where(predicate);
		return entityManager.createQuery(cq).getResultList();
	}

	@Override
	public List<Contact> findByLastName(String lastName) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contact> cq = cb.createQuery(Contact.class);
		Root<Contact> root = cq.from(Contact.class);
		Predicate predicate = cb.equal(root.get("lastName"), lastName);
		cq.where(predicate);
		return entityManager.createQuery(cq).getResultList();
	}

	@Override
	public Contact save(Contact contact) {
		entityManager.persist(contact);
		return contact;
	}

	@Override
	public List<Contact> findAll() {
		CriteriaBuilder criteriaBuilder = entityManager.getCriteriaBuilder();
		CriteriaQuery<Contact> criteriaQuery = criteriaBuilder.createQuery(Contact.class);
		Root<Contact> root = criteriaQuery.from(Contact.class);
		criteriaQuery.select(root);
		return entityManager.createQuery(criteriaQuery).getResultList();
	}

	@Override
	public Contact findById(Long id) {
		return entityManager.find(Contact.class, id);
	}

	@Override
	public void delete(Contact contact) {
		entityManager.remove(contact);
	}

	@Override
	public void deleteById(Long id) {
		Contact contact = findById(id);
		if (contact != null) {
			delete(contact);
		}
	}

}
