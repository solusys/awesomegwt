package com.solusys.gwt.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.solusys.gwt.api.dto.ContactDTO;
import com.solusys.gwt.api.dto.ContactDetails;
import com.solusys.gwt.dao.ContactRepository;
import com.solusys.gwt.model.Contact;
import com.solusys.gwt.service.ContactService;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContactServiceImpl implements ContactService {

	private final ContactRepository contactRepository;

	@Autowired
	public ContactServiceImpl(ContactRepository contactRepository) {
		this.contactRepository = contactRepository;
	}

	@Override
	public ContactDTO createContact(ContactDTO contact) {
		System.out.print("createContact ==>" + contact + "\r");

		contact.firstName = contact.firstName.toUpperCase();
		contact.lastName = contact.lastName.toUpperCase();
		return contact;
	}

	@Override
	public Boolean deleteContact(Long id) {
		System.out.print("deleteContact");
		return true;
	}

	@Override
	public ContactDTO getContact(Long id) {
		System.out.print("getContact ==>" + id + "\r");
		Contact contact = Contact.demoContact();
		contact.setId(id);
		System.out.print("returned ==>" + contact.toContactDTO() + "\r");
		return contact.toContactDTO();
	}

	@Override
	public ArrayList<ContactDetails> deleteContacts(ArrayList<String> ids) {
		System.out.print("deleteContacts");
		return null;
	}

	@Override
	public ArrayList<ContactDetails> getContactDetails() {
		System.out.print("getContactDetails");
		return null;
	}

	@Override
	public ContactDTO updateContact(Long id, ContactDTO contact) {
		System.out.print("updateContact ==>" + contact + "\r");
		return contact;
	}

	@Override
	public List<ContactDTO> getAll() {
		System.out.print("getAll()");

		List<ContactDTO> dtos = new ArrayList<ContactDTO>();
		Contact.randomContacts(10).forEach(c -> dtos.add(c.toContactDTO()));
		return dtos;
	}
}
