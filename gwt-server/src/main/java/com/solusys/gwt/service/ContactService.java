package com.solusys.gwt.service;

import java.util.ArrayList;
import java.util.List;

import com.solusys.gwt.api.dto.ContactDTO;
import com.solusys.gwt.api.dto.ContactDetails;

public interface ContactService {

	ContactDTO createContact(ContactDTO contact);

	Boolean deleteContact(Long id);

	ArrayList<ContactDetails> deleteContacts(ArrayList<String> ids);

	ArrayList<ContactDetails> getContactDetails();

	ContactDTO getContact(Long id);

	ContactDTO updateContact(Long id, ContactDTO contact);

	List<ContactDTO> getAll();
}
