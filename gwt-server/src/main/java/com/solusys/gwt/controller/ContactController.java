/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.solusys.gwt.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.solusys.gwt.api.ContactAPI;
import com.solusys.gwt.api.dto.ContactDTO;
import com.solusys.gwt.service.ContactService;

@RestController
@RequestMapping("/contacts")
public class ContactController {

	@Autowired
	private ContactService contactService;

	@GetMapping
	public List<ContactDTO> getAllContacts() {
		return contactService.getAll();
	}

	@GetMapping("/{id}")
	public ContactDTO getContactById(@PathVariable String id) {
		return contactService.getContact(Long.valueOf(id));
	}

	@PostMapping
	public ContactDTO createContact(@RequestBody ContactDTO contactDTO) {
		return contactService.createContact(contactDTO);
	}

	@PutMapping("/{id}")
	public ContactDTO updateContact(@PathVariable String id, @RequestBody ContactDTO contactDTO) {
		return contactService.updateContact(Long.valueOf(id), contactDTO);
	}

	@DeleteMapping("/{id}")
	public boolean deleteContact(@PathVariable String id) {
		return contactService.deleteContact(Long.valueOf(id));
	}
}