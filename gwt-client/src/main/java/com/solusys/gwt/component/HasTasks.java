package com.solusys.gwt.component;

@FunctionalInterface
public interface HasTasks {
    void update(boolean animate);
}
