package com.solusys.gwt.component.component;

import elemental2.dom.HTMLDivElement;
import org.dominokit.domino.ui.grid.Column;
import org.dominokit.domino.ui.grid.Row;
import org.dominokit.domino.ui.header.BlockHeader;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.layout.EmptyState;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.utils.BaseDominoElement;

import com.google.gwt.core.client.GWT;
import com.solusys.gwt.RestContactClient;
import com.solusys.gwt.api.dto.ContactDTO;

import java.util.logging.Logger;

import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestServiceProxy;

import java.util.ArrayList;
import java.util.List;

import static org.jboss.elemento.Elements.div;

public class ContactDTOCRUDComponent extends BaseDominoElement<HTMLDivElement, ContactDTOCRUDComponent> {

	private int DURATION = 400;
	private final Column column = Column.span8().offset2();
	private HTMLDivElement element = div().element();
	private int delay = 100;
	private String title;
	private final HasContactDTOUiHandlers hasContactDTOUiHandlers;
	private static final String SERVER_CONTEXT_PATH = "http://localhost:8080/";
	private RestContactClient restContactClient;
	private static Logger logger = Logger.getLogger(ContactDTOCRUDComponent.class.getName());

	public ContactDTOCRUDComponent(String title, HasContactDTOUiHandlers hasContactDTOUiHandlers) {

		restContactClient = GWT.create(RestContactClient.class);
		final Resource resource = new Resource(SERVER_CONTEXT_PATH);
		((RestServiceProxy) restContactClient).setResource(resource);

		this.title = title;

		this.hasContactDTOUiHandlers = hasContactDTOUiHandlers;

		element.appendChild(Row.create().appendChild(column).element());
	}

	public static ContactDTOCRUDComponent create(String title, HasContactDTOUiHandlers hasContactDTOUiHandlers) {
		return new ContactDTOCRUDComponent(title, hasContactDTOUiHandlers);
	}

	public ContactDTOCRUDComponent update(boolean animate) {

		List<ContactDTO> contacts = new ArrayList<ContactDTO>();
		restContactClient.getAllContacts(new MethodCallback<List<ContactDTO>>() {

			@Override
			public void onSuccess(Method method, List<ContactDTO> response) {
				response.forEach(contact -> logger.info(contact.toString()));
			}

			@Override
			public void onFailure(Method method, Throwable exception) {
				logger.info("Error: " + exception);
				throw new RuntimeException(exception);
			}
		});

		column.apply(element -> {
			element.appendChild(BlockHeader.create(title));

			if (contacts != null) {
				if (contacts.isEmpty()) {
					element.appendChild(EmptyState.create(Icons.ALL.event_available())
							.setIconColor(Color.GREY_LIGHTEN_1)
							.setTitle("No contacts found")
							.setDescription(
									"If you are a developer then something wrong, you must have something to do.!")
							.styler(style -> style.setMarginTop("10%")));
				} else {
					contacts.forEach(contact -> {
						ContactComponent contactComponent = ContactComponent.create(contact, hasContactDTOUiHandlers);
						if (animate) {
							contactComponent.hide();
							element.appendChild(contactComponent);
							contactComponent.show();
							// Animation.create(contactComponent)
							// .delay(delay)
							// .beforeStart(component -> contactComponent.show())
							// .duration(DURATION)
							// .transition(Transition.SLIDE_IN_UP)
							// .animate();

							delay = delay + DURATION;
							DURATION = 200;
						} else {
							element.appendChild(contactComponent);
						}
					});
				}
			}

		});

		return this;
	}

	@Override
	public HTMLDivElement element() {
		return element;
	}
}
