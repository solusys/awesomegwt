package com.solusys.gwt.component.component;

import elemental2.dom.HTMLDivElement;
import org.dominokit.domino.ui.Typography.Paragraph;
import org.dominokit.domino.ui.animations.Animation;
import org.dominokit.domino.ui.animations.Transition;
import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.dialogs.ConfirmationDialog;
import org.dominokit.domino.ui.forms.FieldsGrouping;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.icons.Icon;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.modals.BaseModal;
import org.dominokit.domino.ui.utils.BaseDominoElement;
import com.solusys.gwt.api.dto.ContactDTO;
import com.solusys.gwt.component.attachments.AttachmentPanelComponent;

public class ContactComponent extends BaseDominoElement<HTMLDivElement, ContactComponent> {

	private final HasContactDTOUiHandlers taskUiHandlers;

	private Card card;
	private ContactDTO task;

	private TextBox firstName;
	private TextBox lastName;
	private TextBox email;

	private AttachmentPanelComponent attachmentPanel;
	private Icon importantIcon;
	
	private FieldsGrouping fieldsGrouping = FieldsGrouping.create();

	public static ContactComponent create(ContactDTO task, HasContactDTOUiHandlers taskUiHandlers) {
		return new ContactComponent(task, taskUiHandlers);
	}

	public ContactComponent(ContactDTO task, HasContactDTOUiHandlers taskUiHandlers) {
		this.task = task;
		this.taskUiHandlers = taskUiHandlers;

		firstName = TextBox.create("First Name").setRequired(true).setAutoValidation(true).groupBy(fieldsGrouping)
				.setPlaceholder("First Name").floating().addLeftAddOn(Icons.ALL.label()).setFixErrorsPosition(true);

		lastName = TextBox.create("Last Name").setRequired(true).setAutoValidation(true).groupBy(fieldsGrouping)
				.setPlaceholder("First Name").floating().addLeftAddOn(Icons.ALL.label()).setFixErrorsPosition(true);

		email = TextBox.create("Email").setRequired(true).setAutoValidation(true).groupBy(fieldsGrouping)
				.setPlaceholder("First Name").floating().addLeftAddOn(Icons.ALL.label()).setFixErrorsPosition(true);

		this.appendChild(firstName);	
		this.appendChild(lastName);		
		this.appendChild(email);	

		init(this);
		update();

	}

	private ConfirmationDialog showConfirmationDialog() {
		return ConfirmationDialog.create("Confirm delete")
				.appendChild(Paragraph.create("Are you sure you want to delete this task?"))
				.apply(element -> element.getFooterElement().styler(style -> style.setBackgroundColor("#f3f3f3")))
				.onConfirm(dialog -> {
					dialog.close();
					Animation.create(ContactComponent.this).transition(Transition.LIGHT_SPEED_OUT).duration(500)
							.callback(element1 -> {
								taskUiHandlers.onContactDelete(task);
							}).animate();

				}).onReject(BaseModal::close).open();
	}

	private void update() {
		// importantIcon.toggleDisplay(Priority.IMPORTANT.equals(task.getPriority()));
		// attachmentPanel.update();
	}

	@Override
	public HTMLDivElement element() {
		return card.element();
	}
}
