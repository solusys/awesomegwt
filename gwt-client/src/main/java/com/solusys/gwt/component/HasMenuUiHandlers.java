package com.solusys.gwt.component;

import com.solusys.gwt.samples.Priority;

public interface HasMenuUiHandlers {
    void onAllSelected();
    void onTodaySelected();
    void onNextWeekSelected();
    void onPrioritySelected(Priority priority);
    void onProjectSelected(String projectName);

    void onListResolved();
	void onContactsSelected();
}
