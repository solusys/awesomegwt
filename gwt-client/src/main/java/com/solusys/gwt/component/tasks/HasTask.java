package com.solusys.gwt.component.tasks;

import com.solusys.gwt.samples.Task;

public interface HasTask {
    Task getTask();
}
