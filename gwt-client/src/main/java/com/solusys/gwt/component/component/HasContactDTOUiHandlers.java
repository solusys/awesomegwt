package com.solusys.gwt.component.component;

import com.solusys.gwt.api.dto.ContactDTO;

public interface HasContactDTOUiHandlers {

	void onContactDelete(ContactDTO task);
	void onEditContact(ContactDTO task);
}
