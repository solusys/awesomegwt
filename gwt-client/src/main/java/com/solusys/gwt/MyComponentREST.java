/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.solusys.gwt;

import java.util.logging.Logger;

import com.google.gwt.user.client.ui.VerticalPanel;



public class MyComponentREST extends VerticalPanel {

	private static Logger logger = Logger.getLogger(MyComponentREST.class.getName());

	private static final String SERVER_CONTEXT_PATH = "http://localhost:8080/";

//	public MyComponentREST() {
//		super();
//
////		PersonDto coolPerson = new PersonDto();
////		coolPerson.setDate(new Date());
////		coolPerson.setName("Lofi");
////		coolPerson.setPersonType(PersonType.COOL);
////
////		PersonDto boringPerson = new PersonDto();
////		boringPerson.setDate(new Date());
////		boringPerson.setName("Test");
////		boringPerson.setPersonType(PersonType.BORING);
////
////		Defaults.setDateFormat(PersonEndpoint.DATE_FORMAT);
////
////
////		Button personListButton = executePersonList(coolPerson);
////		Button personWithErrorListButton = executePersonWithErrorList(boringPerson);
//
//		//add(personListButton);
//		//add(personWithErrorListButton);
//		
//		// my web component
//		
//		
//		
//
//
//		
//	}
//
//	private Button executePersonList(PersonDto person) {
//		Button personListButton = new Button("ZWIN: " + person.getPersonType().name());
//
//		personListButton.addClickHandler(clickEvent -> {
//
//			logger.info("Hello Worlcdcdcdcdcdcdcdcdd: executePersonList");
//
//			// We can use general client without the extension
//			// to RestyGwt RestService
//			PersonClient personClient = GWT.create(RestPersonClient.class);
//			Resource resource = new Resource(SERVER_CONTEXT_PATH);
//			((RestServiceProxy) personClient).setResource(resource);
//
//			personClient.getPersons(new MethodCallback<List<PersonDto>>() {
//				@Override
//				public void onSuccess(Method method, List<PersonDto> response) {
//					response.forEach(person -> logger.info("Person: " + person.getName() + " - Date: "
//							+ person.getDate() + " - Type: " + person.getPersonType()));
//				}
//
//				@Override
//				public void onFailure(Method method, Throwable exception) {
//					logger.info("Error: " + exception);
//					throw new RuntimeException(exception);
//				}
//			});
//		});
//
//		return personListButton;
//	}
//
//	private Button executePersonWithErrorList(PersonDto person) {
//		Button personWithErrorListButton = new Button("Click me: " + person.getPersonType().name());
//
//		personWithErrorListButton.addClickHandler(clickEvent -> {
//			logger.info("Hello World: executePersonWithErrorList");
//
//			DirectRestPersonWithErrorClient personClient = GWT.create(DirectRestPersonWithErrorClient.class);
//			Resource resource = new Resource(SERVER_CONTEXT_PATH);
//			((RestServiceProxy) personClient).setResource(resource);
//
//			// We need to use specific client for RestyGwt
//			// DirectRestService
//			REST.withCallback(new MethodCallback<List<PersonDto>>() {
//				@Override
//				public void onFailure(Method method, Throwable exception) {
//					logger.info("Error: " + exception + "\nMessages: " + method.getResponse().getText());
//				}
//
//				@Override
//				public void onSuccess(Method method, List<PersonDto> response) {
//					response.forEach(person -> logger.info("Person: " + person.getName() + " - Date: "
//							+ person.getDate() + " - Type: " + person.getPersonType()));
//				}
//
//			}).call(personClient).getPersonsWithError();
//		});
//
//		return personWithErrorListButton;
//	}

}