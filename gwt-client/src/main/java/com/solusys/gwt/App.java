package com.solusys.gwt;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.datatable.ColumnConfig;
import org.dominokit.domino.ui.datatable.DataTable;
import org.dominokit.domino.ui.datatable.TableConfig;
import org.dominokit.domino.ui.datatable.TableRow;
import org.dominokit.domino.ui.datatable.store.LocalListDataStore;
import org.dominokit.domino.ui.grid.GridLayout;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.utils.DominoElement;
import org.dominokit.domino.ui.utils.TextNode;
import org.fusesource.restygwt.client.Method;
import org.fusesource.restygwt.client.MethodCallback;
import org.fusesource.restygwt.client.Resource;
import org.fusesource.restygwt.client.RestServiceProxy;

import com.google.gwt.core.client.EntryPoint;
//import com.google.gwt.core.client.GWT;
//import com.google.gwt.user.client.ui.Button;
//import com.google.gwt.user.client.ui.HorizontalPanel;
//import com.google.gwt.user.client.ui.Label;
//import com.google.gwt.user.client.ui.RootPanel;
import com.solusys.gwt.api.dto.ContactDTO;

/**
 * Entry point classes define <code>onModuleLoad()</code>.
 */
public class App implements EntryPoint {

	private static Logger logger = Logger.getLogger(App.class.getName());

	private static final String SERVER_CONTEXT_PATH = "http://localhost:8080/";

	private static final String SERVER_ERROR = "An error occurred while "
			+ "attempting to contact the server. Please check your network " + "connection and try again.";
//
//	public void onModuleLoad1() {
//		final Button createButton = new Button("create()");
//		final Button updateButton = new Button("update()");
//		final Button listButton = new Button("list()");
//		final Button deleteButton = new Button("delete()");
//		final Button getButton = new Button("get()");
//		Button newButton = new Button("New");
//		// final TextBox nameField = new TextBox();
//		// nameField.setText("GWT User");
//		final Label resultLabel = new Label();
//
//		HorizontalPanel vlo = new HorizontalPanel();
//		vlo.setSpacing(1);
//		vlo.add(createButton);
//		vlo.add(updateButton);
//		vlo.add(listButton);
//		vlo.add(deleteButton);
//		vlo.add(getButton);
//		vlo.add(newButton);
//
//		RootPanel.get("myComponent").add(vlo);
//		RootPanel.get("nameFieldContainer").add(resultLabel);
//
//		RestContactClient restContactClient = GWT.create(RestContactClient.class);
//		Resource resource = new Resource(SERVER_CONTEXT_PATH);
//		((RestServiceProxy) restContactClient).setResource(resource);
//
//		listButton.addClickHandler(clickEvent -> {
//
//			logger.info("listing ...");
//
//			restContactClient.getAllContacts(new MethodCallback<List<ContactDTO>>() {
//				@Override
//				public void onSuccess(Method method, List<ContactDTO> response) {
//					response.forEach(contact -> logger.info(contact.toString()));
//				}
//
//				@Override
//				public void onFailure(Method method, Throwable exception) {
//					logger.info("Error: " + exception);
//					throw new RuntimeException(exception);
//				}
//			});
//
//		});
//
//		createButton.addClickHandler(clickEvent -> {
//
//			ContactDTO dto = new ContactDTO();
//			dto.firstName = "Adil";
//			dto.lastName = "Dalli";
//			dto.id = "1234567";
//			dto.emailAddress = "adil@dalli.com";
//
//			logger.info("creating ==> " + dto.toString());
//
//			restContactClient.createContact(dto, new MethodCallback<ContactDTO>() {
//				@Override
//				public void onSuccess(Method method, ContactDTO response) {
//
//					logger.info("Received ==> " + response.toString());
//				}
//
//				@Override
//				public void onFailure(Method method, Throwable exception) {
//					logger.info("Error: " + exception);
//					throw new RuntimeException(exception);
//				}
//			});
//
//		});
//
//		getButton.addClickHandler(clickEvent -> {
//
//			String id = String.valueOf(1234567);
//
//			logger.info("getting ==> " + id);
//
//			restContactClient.getContactById(id, new MethodCallback<ContactDTO>() {
//				@Override
//				public void onSuccess(Method method, ContactDTO response) {
//
//					logger.info("Received ==> " + response.toString());
//				}
//
//				@Override
//				public void onFailure(Method method, Throwable exception) {
//					logger.info("Error: " + exception);
//					throw new RuntimeException(exception);
//				}
//			});
//		});
//	}

	@Override
	public void onModuleLoad() {
		// new DominoDo().run("Domino-Do");

//		HTMLButtonElement button = (HTMLButtonElement) DomGlobal.document.createElement("button");
//		button.textContent = "Click me!";
//		//DomGlobal.document.body.appendChild(button);

		List<ContactDTO> contacts = new ArrayList<>();

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));
		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));
		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));
		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		contacts.add(new ContactDTO("1", "John", "Doe", "john.doe@example.com"));
		contacts.add(new ContactDTO("2", "Jane", "Doe", "jane.doe@example.com"));

		TableConfig<ContactDTO> tableConfig = new TableConfig<ContactDTO>();
		tableConfig.addColumn(ColumnConfig.<ContactDTO>create("id", "ID").textAlign("center")
				.setCellRenderer(cell -> TextNode.of(cell.getTableRow().getRecord().getId())));
		tableConfig.addColumn(ColumnConfig.<ContactDTO>create("firstName", "First name")
				.setCellRenderer(cell -> TextNode.of(cell.getTableRow().getRecord().getFirstName())));
		tableConfig.addColumn(ColumnConfig.<ContactDTO>create("lastName", "Last name")
				.setCellRenderer(cell -> TextNode.of(cell.getTableRow().getRecord().getFirstName())));
		tableConfig.addColumn(ColumnConfig.<ContactDTO>create("email", "Email")
				.setCellRenderer(cell -> TextNode.of(cell.getTableRow().getRecord().getFirstName())));

//		tableConfig.addColumn(ColumnConfig.<ContactDTO>create("actions", "Actions")
//				.setCellRenderer(cell -> {
//					
//					TableRow<ContactDTO> row = cell.getTableRow();
//					ContactDTO contact = row.getRecord();
//		
//					Button edit = Button.createPrimary("PRIMARY");
//					edit.style().setMargin("5px").setMinWidth("120px");
//					Button delete = Button.createPrimary("DEFAULT");
//					delete.style().setMargin("5px").setMinWidth("120px");
//					edit.addEventListener("click", evt -> {
//						// handle edit button click event
//					});
//					delete.addEventListener("click", evt -> {
//						// handle delete button click event
//					});
//					
//					GridLayout gridLayout = GridLayout.create()
//			                .style()
//			                .setHeight("260px").get();
//					
//					gridLayout.getContentElement().appendChild(edit);
//					gridLayout.getHeaderElement().appendChild(delete);
//					
//					return FragmentNode.of(edit, delete);
//		}));
		
		
	    tableConfig
        .addColumn(
        		
            ColumnConfig.<ContactDTO>create("edit_save", "")
                .setCellRenderer(
                    cell ->
                        Icons.ALL
                            .pencil_mdi()
                            .clickable()
                            .setTooltip("Edit")
                            .addClickListener(evt -> cell.getTableRow().edit())
                            .element())
//                .setEditableCellRenderer(
//                    cell ->
//                        Icons.ALL
//                            .content_save_mdi()
//                            .clickable()
//                            .setTooltip("Save")
//                            .addClickListener(
//                                evt -> {
//                                  if (cell.getTableRow().validate().isValid()) {
//                                    cell.getTableRow().save();
//                                  }
//                                })
//                            .element())
                
        		);


		LocalListDataStore<ContactDTO> localListDataStore = new LocalListDataStore<>();
		DataTable<ContactDTO> table = new DataTable<>(tableConfig, localListDataStore);
//		DominoElement.body().appendChild(
//				Card.create("BASIC TABLE", "By default a table will auto fit columns and allow custom cell content")
//						.setCollapsible().appendChild(new TableStyleActions(table)).appendChild(table).element());
		localListDataStore.setData(contacts);

		DominoElement.body().appendChild(table);
	}

}
