package com.solusys.gwt.awesomegwt.ui;

import com.solusys.gwt.samples.Priority;

public interface HasMainMenuUiHandlers {
    void onAllSelected();
    void onPrioritySelected(Priority priority);
}
