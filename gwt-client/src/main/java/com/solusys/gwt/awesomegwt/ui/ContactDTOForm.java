package com.solusys.gwt.awesomegwt.ui;

import org.dominokit.domino.ui.cards.Card;
import org.dominokit.domino.ui.cards.HeaderAction;
import org.dominokit.domino.ui.forms.TextBox;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.utils.BaseDominoElement;
import org.dominokit.domino.ui.utils.ScreenMedia;

import com.solusys.gwt.api.dto.ContactDTO;
import elemental2.dom.HTMLDivElement;

public class ContactDTOForm extends BaseDominoElement<HTMLDivElement, ContactDTOForm> {

	private Card card;
    private TextBox idBox = TextBox.create("ID").setRequired(true);
    private TextBox firstNameBox = TextBox.create("First Name").setRequired(true);
    private TextBox lastNameBox = TextBox.create("Last Name").setRequired(true);
    private TextBox emailBox = TextBox.create("Email Address").setRequired(true);

	//private final ColorScheme projectColor;
	
    public ContactDTOForm() {
        
		card = Card.create("dto")
				//.styler(style -> style.setCssProperty("border-left", "5px solid " + projectColor.color().getHex()))
				.addHeaderAction(HeaderAction.create(Icons.ALL.save().setTooltip("Save"))
						.hideOn(ScreenMedia.SMALL_AND_DOWN).addClickListener(evt -> save()))
				.appendChild(idBox)
				.appendChild(firstNameBox)
				.appendChild(lastNameBox)
				.appendChild(emailBox);
		

    }

    private Object save() {
		// TODO Auto-generated method stub
		return null;
	}

	public ContactDTO getContactDTO() {
        ContactDTO contactDTO = new ContactDTO();
        contactDTO.setId(idBox.getValue());
        contactDTO.setFirstName(firstNameBox.getValue());
        contactDTO.setLastName(lastNameBox.getValue());
        contactDTO.setEmailAddress(emailBox.getValue());
        return contactDTO;
    }

    public void setContactDTO(ContactDTO contactDTO) {
        idBox.setValue(contactDTO.getId());
        firstNameBox.setValue(contactDTO.getFirstName());
        lastNameBox.setValue(contactDTO.getLastName());
        emailBox.setValue(contactDTO.getEmailAddress());
    }

	@Override
	public HTMLDivElement element() {
		return card.element();
	}

}
