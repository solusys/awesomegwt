package com.solusys.gwt.awesomegwt.ui;

import elemental2.dom.HTMLDivElement;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.tree.Tree;
import org.dominokit.domino.ui.tree.TreeItem;
import org.dominokit.domino.ui.utils.BaseDominoElement;

import com.solusys.gwt.samples.Priority;

public class LeftPanel extends BaseDominoElement<HTMLDivElement, LeftPanel> {
	private final Tree<String> menu = Tree.create();

	public static LeftPanel create(HasMainMenuUiHandlers menuUiHandlers) {
		return new LeftPanel(menuUiHandlers);
	}

	public LeftPanel(HasMainMenuUiHandlers menuUiHandlers) {

		menu.apply(element -> element.getRoot().style().addCss("menu-flow"))
				.appendChild(TreeItem.create("All Tasks", Icons.ALL.inbox())
						.addClickListener(evt -> menuUiHandlers.onAllSelected()))

				.addSeparator()

				.appendChild(TreeItem.create("Important", Icons.ALL.priority_high().setColor(Color.RED))
						.addClickListener(evt -> menuUiHandlers.onPrioritySelected(Priority.IMPORTANT))
						);
	}

	@Override
	public HTMLDivElement element() {
		return menu.element();
	}
}
