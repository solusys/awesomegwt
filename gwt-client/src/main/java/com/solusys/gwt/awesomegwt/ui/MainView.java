package com.solusys.gwt.awesomegwt.ui;

import java.util.List;

import org.dominokit.domino.ui.button.Button;
import org.dominokit.domino.ui.icons.Icons;
import org.dominokit.domino.ui.layout.Layout;
import org.dominokit.domino.ui.layout.LayoutActionItem;
import org.dominokit.domino.ui.search.Search;
import org.dominokit.domino.ui.style.Color;
import org.dominokit.domino.ui.style.ColorScheme;
import org.dominokit.domino.ui.utils.DominoElement;

import com.google.gwt.core.client.GWT;
import com.solusys.gwt.component.DominoDo;
import com.solusys.gwt.component.tasks.TasksList;
import com.solusys.gwt.component.tasks.TasksRepository;
import com.solusys.gwt.samples.Priority;
import com.solusys.gwt.samples.Task;

public class MainView implements HasMainMenuUiHandlers {
	private Layout layout;
	// private Search search;

	private final TasksRepository tasksRepository = new TasksRepository();
	private TaskCRUDComponent taskCRUDComponent;

	public MainView(String title) {

		// search = Search.create();

		layout = Layout.create(title);
		layout
				// .apply(self -> self.getNavigationBar().insertBefore(search,
				// layout.getNavigationBar().firstChild()))
				.apply(self -> self.getLeftPanel().appendChild(new LeftPanel(this)))
				.apply(self -> self.getRightPanel().appendChild(new SettingsPanel()))
				.apply(self -> self.getTopBar().appendChild(
						LayoutActionItem.create(Icons.ALL.settings()).addClickListener(evt -> layout.showRightPanel()))
				// .appendChild(LayoutActionItem.create(Icons.ALL.search()).addClickListener(evt
				// -> search.open()))
				).autoFixLeftPanel()
				// .setLogo(img("./todo.png"))
				.show(ColorScheme.BLUE);

		Button addButton = Button.create(Icons.ALL.add()).setBackground(Color.THEME).setContent("ADD TASK")
				.styler(style -> style.addCss("add-button")).addClickListener(evt -> GWT.log("add button clicked"));

		DominoElement.body().appendChild(addButton.element());

	}

	@Override
	public void onAllSelected() {
		List<Task> tasks = tasksRepository.listAll();
		taskCRUDComponent = new TaskCRUDComponent("All Tasks", tasks, this);
		layout.setContent(taskCRUDComponent);
		taskCRUDComponent.update(true);
	}

	@Override
	public void onPrioritySelected(Priority priority) {
		// TODO Auto-generated method stub

	}
}
