function MyElement() {
  this.element = document.createElement('div');
  this.label = document.createElement('label');
  this.label.innerHTML = 'My Label:';
  this.element.appendChild(this.label);
  this.textfield = document.createElement('input');
  this.textfield.type = 'text';
  this.element.appendChild(this.textfield);
}

MyElement.prototype.getElement = function() {
  return this.element;
};

MyElement.prototype.getValue = function() {
  return this.textfield.value;
};

MyElement.prototype.setValue = function(value) {
  this.textfield.value = value;
};