package com.solusys.gwt.api;

import java.util.List;

/**
 * 
 * @author adildalli
 *
 * @param <DTO>
 * 
 */
public interface RestApi<DTO,Filter> {

	  public List<DTO> getAllDTOs(Filter filter);
	  public DTO createDTO(DTO dto);
	  public DTO updateDTO(DTO dto);
	  public boolean deleteDTO(DTO dto);
	  public boolean deleteAllDTOs(List<DTO> dtos);

}
