/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package com.solusys.gwt.api;

import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;

import com.solusys.gwt.api.dto.ContactDTO;

public interface ContactAPI {

	List<ContactDTO> getAllContacts();

	ContactDTO getContactById(@PathVariable Long id);

	ContactDTO createContact(@RequestBody ContactDTO contactDTO);

	ContactDTO updateContact(@PathVariable Long id, @RequestBody ContactDTO contactDTO);

	boolean deleteContact(@PathVariable Long id);

}
