package com.solusys.gwt;

public class Constants  {
	public static final String DOMINO_UI = "Domino UI";
	public static final String NALU_MVP = "Nalu MVP";
	public static final String GWT = "GWT";
	public static final String MOVIES = "Movies";
	public static final String CONTACT_LIST = "app/contacts";
	public static final String CONTACT_WITH_ERROR_LIST = "app/contacts/error";
	public static final String DATE_FORMAT = "yyyy-MM-dd@HH:mm:ss.SSSZ";
}
