package com.solusys.gwt.samples;

public enum Status {

    ACTIVE, COMPLETED;
}
